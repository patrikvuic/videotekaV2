import { MoviesInteractorImpl } from "../interactors/movie/moviesInteractorImpl"
import { NewRentInteractorImpl } from "../interactors/newRent/newRentInteractorImpl"
import { RentInteractorImpl } from "../interactors/rent/rentInteractorImpl"
import { RentsInteractorImpl } from "../interactors/rents/rentsInteractorImpl"
import { UserInteractorImpl } from "../interactors/user/userInteractorImpl"
import { MoviesRepositoryImpl } from "../repository/movies/moviesRepositoryImpl"
import { MoviesRepositoryMock } from "../repository/movies/moviesRepositoryMock"
import { RentsRepositoryImpl } from "../repository/rents/rentsRepositoryImpl"
import { RentsRepositoryMock } from "../repository/rents/rentsRepositoryMock"
import { UsersRepositoryImpl } from "../repository/users/userRepositoryImpl"
import { UsersRepositoryMock } from "../repository/users/userRepositoryMock"

export type DI = {
  interactors: Record<string, object>,
  repositories: Record<string, object>,
}

const mockRepos = {
  rentsRepository: new RentsRepositoryMock(),
  userRepository: new UsersRepositoryMock(),
  moviesRepository: new MoviesRepositoryMock(),
}

const repos = {
  rentsRepository: new RentsRepositoryImpl(),
  userRepository: new UsersRepositoryImpl(),
  moviesRepository: new MoviesRepositoryImpl(),
}

const interactors = {
  rentsInteractor: new RentsInteractorImpl(repos.rentsRepository),
  rentInteractor: new RentInteractorImpl(repos.rentsRepository),
  newRentInteractor: new NewRentInteractorImpl(repos.rentsRepository),
  userInteractor: new  UserInteractorImpl(repos.userRepository),
  moviesInteractor: new MoviesInteractorImpl(repos.moviesRepository),
}

export const di: DI = {
  repositories: repos,
  interactors: interactors,
}