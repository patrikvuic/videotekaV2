import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";
import '../App.css';
import Base from '../components/base/Base';
import NewRentScreen from "../components/newRent/NewRentScreen";
import RentScreen from "../components/rent/RentScreen";
import RentsScreen from '../components/rents/RentsScreen';
import Route404 from '../components/Route404';

export default function AppRouter() {

  return (
    <Router>
      <Base>
        <Routes>
          <Route path="rents" element={<RentsScreen />} >
            <Route path=":id" element={<RentScreen />} />
            <Route path="create" element={<NewRentScreen />} />
          </Route>
          <Route path="*" element={<Route404 />} />
        </Routes>
      </Base>
    </Router >
  );
}
