import { Movie } from "../../models/Movie";
import { MoviesRepository } from "../../repository/movies/moviesRepository";
import { MoviesInteractor } from "./moviesInteractor";

export class MoviesInteractorImpl implements MoviesInteractor {
  private readonly repository: MoviesRepository;

  constructor(repo: MoviesRepository) {
    this.repository = repo;

    this.fetchMovies=this.fetchMovies.bind(this);
  }

  fetchMovies(): Promise<Movie[]> {
    return this.repository.fetchMovies();
  }
}