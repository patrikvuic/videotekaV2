import { Movie } from "../../models/Movie";

export interface MoviesInteractor {
  fetchMovies: () => Promise<Movie[]>,
}