import { Rent } from "../../models/Rent";
import { RentInfo } from "../../models/RentInfo";
import { RentsRepository } from "../../repository/rents/rentsRepository";
import { RentInteractor } from "./rentInteractor";

export class RentInteractorImpl implements RentInteractor {
  private readonly repository: RentsRepository;

  constructor(repo: RentsRepository) {
    this.repository = repo;

    this.fetchRent.bind(this);
    this.deleteRent.bind(this);
    this.updateRent.bind(this);
  }

  fetchRent(id: number): Promise<Rent> {
    return this.repository.fetchRent(id);

  }

  deleteRent(id: number): Promise<void> {
    return this.repository.deleteRent(id);
  }

  updateRent(id: number, info: RentInfo): Promise<void> {
    return this.repository.updateRent(id, info);
  }
}