import { Rent } from "../../models/Rent";
import { RentInfo } from "../../models/RentInfo";

export interface RentInteractor {
  fetchRent: (id: number) => Promise<Rent>,
  deleteRent: (id: number) => Promise<void>,
  updateRent: (id: number, info: RentInfo) => Promise<void>,
}