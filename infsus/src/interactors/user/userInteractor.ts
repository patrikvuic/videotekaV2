import { UserType } from "../../constants";
import { User } from "../../models/User";

export interface UserInteractor {
  fetchUsers(role?: UserType): Promise<User[]>;
}