import { UserType } from "../../constants";
import { User } from "../../models/User";
import { UsersRepository } from "../../repository/users/usersRepository";
import { UserInteractor } from "./userInteractor";

export class UserInteractorImpl implements UserInteractor {
  private readonly repository: UsersRepository;

  constructor(repo: UsersRepository) {
    this.repository = repo;

    this.fetchUsers.bind(this);
  }

  fetchUsers(role: UserType): Promise<User[]> {
    return this.repository.fetchUsers(role);
  }
}