import { RentInfo } from "../../models/RentInfo";

export interface NewRentInteractor {
  createRent: (info: RentInfo) => Promise<void>,
}