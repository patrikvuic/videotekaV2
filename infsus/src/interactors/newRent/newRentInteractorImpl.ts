import { RentInfo } from "../../models/RentInfo";
import { RentsRepository } from "../../repository/rents/rentsRepository";
import { NewRentInteractor } from "./newRentInteractor";

export class NewRentInteractorImpl implements NewRentInteractor {
  private readonly repository: RentsRepository;

  constructor(repo: RentsRepository) {
    this.repository = repo;

    this.createRent.bind(this);
  }

  createRent(info: RentInfo) {
    return this.repository.createRent(info);
  }
}