import { RentsWithInfo } from "../../models/RentsWithInfo";
import { RentsRepository } from "../../repository/rents/rentsRepository";
import { RentsInteractor } from "./rentsInteractor";

export class RentsInteractorImpl implements RentsInteractor {
  private readonly repository: RentsRepository;

  constructor(repo: RentsRepository) {
    this.repository = repo;

    this.fetchRents.bind(this);
  }

  fetchRents(page: number, count: number, activity: boolean): Promise<RentsWithInfo> {
    return this.repository.fetchRents(page, count, activity);
  }
}