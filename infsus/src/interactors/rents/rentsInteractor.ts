import { RentsWithInfo } from "../../models/RentsWithInfo";

export interface RentsInteractor {
  fetchRents: (page: number, count: number, activity: boolean) => Promise<RentsWithInfo>,
}