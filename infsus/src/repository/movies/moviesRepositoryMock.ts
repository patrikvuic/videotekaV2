import { Movie } from "../../models/Movie";
import { wait } from "../../util";
import { movies } from "./mockData";
import { MoviesRepository } from "./moviesRepository";

export class MoviesRepositoryMock implements MoviesRepository {
  async fetchMovies(): Promise<Movie[]> {
    await wait(1000);
    return movies;
  }
}