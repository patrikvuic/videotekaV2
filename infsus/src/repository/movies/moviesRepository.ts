import { Movie } from "../../models/Movie";

export interface MoviesRepository {
  fetchMovies(): Promise<Movie[]>
}