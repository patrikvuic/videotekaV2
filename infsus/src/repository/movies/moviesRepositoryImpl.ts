import { API } from "../../constants";
import { Movie } from "../../models/Movie";
import { MoviesRepository } from "./moviesRepository";

export class MoviesRepositoryImpl implements MoviesRepository {
  async fetchMovies(): Promise<Movie[]> {
    const res = await fetch(API.movies());

    if(!res.ok) throw Error('Ups, something went wrong');

    return await res.json();
  }
}