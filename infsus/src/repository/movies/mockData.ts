import { Movie } from "../../models/Movie";

export const movies: Movie[] = [
  {
    movieId: 1,
    title: 'Movie 1',
    description: 'Some stupid string',
    actors: ['actor actores', 'miko mikores'],
    thumbnailUrl: '',
    priceByDay: 120,
    genre: 'genre'
  },
  {
    movieId: 2,
    title: 'Movie 2',
    description: 'Some stupid string',
    actors: ['actor actores', 'miko mikores'],
    thumbnailUrl: '',
    priceByDay: 10,
    genre: 'genre'
  },
  {
    movieId: 3,
    title: 'Movie 3',
    description: 'Some stupid string',
    actors: ['actor actores', 'miko mikores'],
    thumbnailUrl: '',
    priceByDay: 200,
    genre: 'genre'
  },
  {
    movieId: 4,
    title: 'Movie 4',
    description: 'Some stupid string',
    actors: ['actor actores', 'miko mikores'],
    thumbnailUrl: '',
    priceByDay: 120,
    genre: 'genre'
  },
  {
    movieId: 5,
    title: 'Movie 5',
    description: 'Some stupid string',
    actors: ['actor actores', 'miko mikores'],
    thumbnailUrl: '',
    priceByDay: 1200,
    genre: 'genre'
  },
  {
    movieId: 6,
    title: 'Movie 6',
    description: 'Some stupid string',
    actors: ['actor actores', 'miko mikores'],
    thumbnailUrl: '',
    priceByDay: 120,
    genre: 'genre'
  }
];