import { UserType } from "../../constants";
import { User } from "../../models/User";

export interface UsersRepository {
  fetchUsers(role: UserType): Promise<User[]>
}