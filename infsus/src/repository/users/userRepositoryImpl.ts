import { API, UserType } from "../../constants";
import { User, UserLean } from "../../models/User";
import { UsersRepository } from "./usersRepository";

export class UsersRepositoryImpl implements UsersRepository {
  async fetchUsers(role: UserType): Promise<User[]> {
    const res = await fetch(API.users(role));

    if(!res.ok) throw Error('Ups, something went wrong');

    return (await res.json() as UserLean[]).map(ul => ({ ...ul, role }));
  }
}