import { UserLean } from "../../models/User";

export const users: UserLean[] = [
  {
    firstName: 'Nikola',
    lastName: 'Mikic',
    email: 'n@fer.hr',
  },
  {
    firstName: 'Mirko',
    lastName: 'Mikic',
    email: 'M@der.hr',
  },
  {
    firstName: 'Slavko',
    lastName: 'Kikic',
    email: 's@fer.hr',
  },
  {
    firstName: 'Jurica',
    lastName: 'Lukic',
    email: 'j@fer.hr',
  },
  {
    firstName: 'Mislav',
    lastName: 'Bago',
    email: 'm@fer.hr',
  },
  {
    firstName: 'Lukaku',
    lastName: 'Lukic',
    email: 'lu@fer.hr',
  },
  {
    firstName: 'Kiki',
    lastName: 'Kiko',
    email: 'k@fer.j',
  },
  {
    firstName: 'Slavko',
    lastName: 'Kikic',
    email: 's1@fer.hr',
  },
  {
    firstName: 'Jurica',
    lastName: 'Lukic',
    email: 'j1@fer.hr',
  },
  {
    firstName: 'Mislav',
    lastName: 'Bago',
    email: 'm1@fer.hr',
  },
  {
    firstName: 'Lukaku',
    lastName: 'Lukic',
    email: 'lu1@fer.hr',
  },
  {
    firstName: 'Kiki',
    lastName: 'Kiko',
    email: 'k1@fer.j',
  },
];