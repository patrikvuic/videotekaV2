import { UserType } from "../../constants";
import { User } from "../../models/User";
import { wait } from "../../util";
import { users } from "./mockData";
import { UsersRepository } from "./usersRepository";

export class UsersRepositoryMock implements UsersRepository {
  async fetchUsers(role: UserType): Promise<User[]> {
    await wait(1000);
    return users.map(ul => ({ ...ul, role }));
  }
}