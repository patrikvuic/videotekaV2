import { Rent } from "../../models/Rent";
import { RentInfo } from "../../models/RentInfo";
import { RentsWithInfo } from "../../models/RentsWithInfo";
import { RentsRepository } from "../../repository/rents/rentsRepository";
import { wait } from "../../util";
import { users } from "../users/mockData";
import { data } from "./mockData";

export class RentsRepositoryMock implements RentsRepository {

  constructor() {
    this.fetchRents.bind(this);
    this.fetchRent.bind(this);
    this.deleteRent.bind(this);
    this.updateRent.bind(this);
  }

  async fetchRents(page: number, count: number, activity: boolean): Promise<RentsWithInfo> {
    await wait(1000);
    const arr = data.filter(r => activity ? !r.dateTo : true);
    const res = arr.slice((page - 1) * count, (page - 1) * count + count);

    return { content: res, totalElements: arr.length };
  }

  async fetchRent(id: number): Promise<Rent> {
    await wait(1500);
    return data[id-1];
  }

  async deleteRent(id: number): Promise<void> {
    await wait(1000);
    data.splice(id-1, 1);
  }

  async updateRent(id: number, info: RentInfo): Promise<void> {
    await wait(1000);
    var buyer = data[id-1].buyer;
    var employee = data[id-1].employee;
    var movies = data[id-1].movies;
    if(info.buyer) {
      for(var b of users) {
        if(b.email === info.buyer) {
          buyer = b;
          break;
        }
      }
    }
    if(info.employee) {
      for(var e of users) {
        if(e.email === info.employee) {
          employee = e;
          break;
        }
      }
    }
    if(info.movies) {
      movies = [];
      for(var m of info.movies) {
        for(var mov of movies) {
          if(m === mov.movieId) {
            movies.push(mov);
            break;
          }
        }
      }
    }
    data[id-1] = {
      ...data[id-1],
      dateFrom: new Date(info.dateFrom),
      dateTo: info.dateTo ? new Date(info.dateFrom) : null,
      buyer,
      employee,
      movies
    };
  }

  async createRent(info: RentInfo): Promise<void> {
      data.push(data[0]);
  }
}