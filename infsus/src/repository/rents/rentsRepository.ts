import { Rent } from "../../models/Rent";
import { RentInfo } from "../../models/RentInfo";
import { RentsWithInfo } from "../../models/RentsWithInfo";

export interface RentsRepository {
  fetchRents(page: number, count: number, activity: boolean): Promise<RentsWithInfo>,
  createRent(info: RentInfo): Promise<void>,
  fetchRent(id: number): Promise<Rent>,
  deleteRent(id: number): Promise<void>,
  updateRent(id: number, info: RentInfo): Promise<void>,
}