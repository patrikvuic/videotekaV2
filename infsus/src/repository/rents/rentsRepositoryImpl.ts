import moment from "moment";
import { API } from "../../constants";
import { Rent } from "../../models/Rent";
import { RentInfo } from "../../models/RentInfo";
import { Rents } from "../../models/Rents";
import { RentsWithInfo } from "../../models/RentsWithInfo";
import { RentsRepository } from "../../repository/rents/rentsRepository";

export class RentsRepositoryImpl implements RentsRepository {

  constructor() {
    this.fetchRents.bind(this);
    this.fetchRent.bind(this);
    this.deleteRent.bind(this);
    this.updateRent.bind(this);
  }

  private static mapRent(rent: Rent): Rent;
  private static mapRent(rent: Rents): Rents;
  private static mapRent(rent: Rents | Rent): Rents | Rent {
    rent.dateFrom = new Date(rent.dateFrom);
    if(rent.dateTo !== null) {
      rent.dateTo = new Date(rent.dateTo);
    }
    return rent;
  }

  async fetchRents(page: number, count: number, activity: boolean): Promise<RentsWithInfo> {
    const res = await fetch(API.rents(page, count, activity));

    if(!res.ok) throw Error('Ups, something went wrong!');

    const json: RentsWithInfo = await res.json();
    json.content = json.content.map(r => RentsRepositoryImpl.mapRent(r));

    return json;
  }

  async fetchRent(id: number): Promise<Rent> {
    const res = await fetch(API.rent(id));

    if(!res.ok) throw Error('Ups, something went wrong!');

    return RentsRepositoryImpl.mapRent(await res.json());
  }

  async deleteRent(id: number): Promise<void> {
    const res = await fetch(API.rent(id), {method: 'DELETE'});

    if(!res.ok) throw Error('Ups, something went wrong!');

    return await res.json();
  }

  async updateRent(id: number, info: RentInfo): Promise<void> {
    const from = moment(info.dateFrom, 'YYYY-MM-DD');

    if(from.isAfter(moment.now())) throw Error('Can\'t create rent in the future');

    if(info.dateTo != null) {
      const to = moment(info.dateTo, 'YYYY-MM-DD');
      if(to.isBefore(from)) throw Error('Return date can\'t be before rent date');
    }
    const res = await fetch(API.rent(id), {headers:{'content-type': 'application/json'}, method: 'PUT', body: JSON.stringify(info)});

    if(!res.ok) throw Error('Ups, something went wrong!');

    return;
  }

  async createRent(info: RentInfo) {
    const res = await fetch(API.emptyRents(),{headers:{'content-type': 'application/json'}, method: 'POST', body: JSON.stringify(info)});

    if(!res.ok) throw Error('Ups, something went wrong!');

    return;
  }

}