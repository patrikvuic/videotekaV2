import { UserLean } from "./User"

export type Rents = {
  rentId: number,
	dateFrom: Date,
  dateTo: Date | null,
  totalPrice?: number,
  priceByDay: number,
  buyer: UserLean,
  employee: UserLean,
  numberOfMovies: number,
}