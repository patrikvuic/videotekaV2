import { Rents } from "./Rents"

export type RentsWithInfo = {
  content: Rents[],
  totalElements: number
}