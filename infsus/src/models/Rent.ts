import { Movie } from '../models/Movie';
import { UserLean } from './User';

export type Rent = {
  rentId: number,
	dateFrom: Date,
  dateTo: Date | null,
  totalPrice?: number,
  priceByDay: number,
  buyer: UserLean,
  employee: UserLean,
  movies: Movie[],
  numberOfMovies: number,
}