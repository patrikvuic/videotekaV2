export type Movie = {
  movieId: number,
  title: string,
  description: string,
  actors: string[],
  thumbnailUrl: string,
  priceByDay: number,
  genre: string
}