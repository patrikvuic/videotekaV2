import { UserType } from "../constants"

export type User = UserLean & {
  role: UserType
}

export type UserLean = {
  email: string,
  firstName: string,
  lastName: string,
}