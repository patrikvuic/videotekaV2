export type RentInfo = {
	dateFrom: string,
  dateTo: string | null,
  buyer: string,
  employee: string,
  movies: number[]
}