import './App.css';
import 'antd/dist/antd.css';
import AppRouter from './router/Router';

function App() {
  return (
    // <AppContext.Provider value={{rootReducer}}>
      <AppRouter />
    // </AppContext.Provider>
  );
}

export default App;
