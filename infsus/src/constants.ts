const base = 'http://localhost:8080';

export const API = {
  movies: () => `${base}/movies`,
  users: (role?: string) => `${base}/users${!!role && `?role=${role}`}`,
  rents: (page: number, count: number, activity: boolean) => `${base}/rents?page=${page-1}&size=${count}&activeOnly=${activity}`,
  emptyRents: () => `${base}/rents`,
  rent: (id: number) => `${base}/rents/${id}`
}

export enum UserType {
  buyer = 'buyer',
  employee = 'employee'
}