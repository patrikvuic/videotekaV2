import { Button, Card, DatePicker, Divider, Popover, Select, Space, Spin } from "antd";
import { useMemo } from "react";
import { useNavigate } from "react-router-dom";
import { CloseOutlined } from '@ant-design/icons';
import { styles } from "./styles/styles";
import { useProvider } from "../../provider/useProvider";
import { newRentScreenProvider } from "./provider/newRentScreenProvider";
import moment from "moment";

function NewRentScreen() {
  const navigate = useNavigate();

  const provider = useProvider(newRentScreenProvider, (prov) => prov.init());

  const title = useMemo(() => {
    return <div style={styles.title}>
      <span style={styles.spanTitle}>New rent</span>
      <Button
        shape="circle"
        icon={<CloseOutlined />}
        onClick={() => navigate('/rents')}
      />
    </div>
  }, [navigate]);

  async function onSave() {
    await provider.onSaveClicked();

    navigate('/rents');
  }

  return (
    <div style={styles.modal} id="modal">
      <Card style={styles.card} title={title}>
        <Card.Grid hoverable={false} style={styles.halfCell}>
          <Divider style={styles.divider} orientation="left">Date from</Divider>
          <DatePicker
            onChange={provider.onDateFromChanged}
            defaultValue={moment(provider.rent.dateFrom.toLocaleDateString('HR'), 'DD.MM.YYYY.')}
            format={'DD.MM.YYYY.'}
            allowClear={false}
          />
        </Card.Grid>
        <Card.Grid hoverable={false} style={styles.halfCell}>
          <Divider style={styles.divider} orientation="left">Buyer</Divider>
          <Select
            value={!!provider.rent.buyer.email ? provider.rent.buyer.email : undefined}
            onChange={provider.onBuyerChanged}
            style={styles.select}
            placeholder="Please select"
          >
            {provider.buyers.length === 0 ? (
              <Select.Option value={provider.rent.buyer.email} >
                {`${provider.rent.buyer.firstName} ${provider.rent.buyer.lastName}`}
              </Select.Option>
            ) : (
              provider.buyers.map(buyer => (
                <Select.Option key={buyer.email} value={buyer.email} >
                  {`${buyer.firstName} ${buyer.lastName}`}
                </Select.Option>
              ))
            )}
          </Select>
        </Card.Grid>
        <Card.Grid hoverable={false} style={styles.halfCell}>
          <Divider style={styles.divider} orientation="left">Price (per day)</Divider>
          {provider.rent.priceByDay} HRK
        </Card.Grid>
        <Card.Grid hoverable={false} style={styles.halfCell}>
          <Divider style={styles.divider} orientation="left">Employee</Divider>
          <Select
            value={!!provider.rent.employee.email ? provider.rent.employee.email : undefined}
            onChange={provider.onEmployeeChanged}
            style={styles.select}
            placeholder="Please select"
          >
            {provider.employees.length === 0 ? (
              <Select.Option value={provider.rent.employee.email} >
                {`${provider.rent.employee.firstName} ${provider.rent.employee.lastName}`}
              </Select.Option>
            ) : (
              provider.employees.map(employee => (
                <Select.Option key={employee.email} value={employee.email} >
                  {`${employee.firstName} ${employee.lastName}`}
                </Select.Option>
              ))
            )}
          </Select>
        </Card.Grid>
        <Card.Grid hoverable={false} style={styles.wideCell}>
          <Divider style={styles.divider} orientation="left">Movies</Divider>
          <Select
            mode="multiple"
            style={{ width: '100%' }}
            value={provider.rent.movies.map(mov => mov.movieId)}
            placeholder="Please select"
            onChange={provider.onMoviesChanged}
          >
            {provider.movies.map(mov => (
                <Select.Option key={mov.movieId} value={mov.movieId} >
                <Popover content={(
                    <div>
                      <div>Id: {mov.movieId}</div>
                      <div>Genre: {mov.genre}</div>
                      <div>
                        <h5>Description:</h5>
                        <p>{mov.description}</p>
                      </div>
                      <div>
                        <span>Actors:</span>
                        <ul>
                          {mov.actors.map(a => (
                            <li>{a}</li>
                          ))}
                        </ul>
                      </div>
                      <div>Price per day: {mov.priceByDay}</div>
                    </div>
                  )}
                  title={mov.title}
                >
                  {mov.title}
                </Popover>
              </Select.Option>
            ))}
          </Select>
        </Card.Grid>
        <Card.Grid hoverable={false} style={styles.wideCellFlex}>
          <Space align="center">
            <Button onClick={() => navigate('/rents')}>Cancel</Button>
            <Button onClick={onSave} type="primary" disabled={provider.isSaveDisabled}>Save</Button>
          </Space>
        </Card.Grid>
        {provider.loading && <Loader />}
      </Card>
    </div>
  );
}

function Loader() {
  return (
    <div style={styles.loaderDiv}>
      <Spin size="large" />
    </div>
  );
}

export default NewRentScreen;