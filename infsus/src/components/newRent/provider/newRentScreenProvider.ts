import { di } from "../../../di/di";
import { Provider } from "../../../provider/provider"
import { Rent } from "../../../models/Rent";
import moment from 'moment';
import { UserInteractor } from "../../../interactors/user/userInteractor";
import { User } from "../../../models/User";
import { UserType } from "../../../constants";
import { MoviesInteractor } from "../../../interactors/movie/moviesInteractor";
import { Movie } from "../../../models/Movie";
import { RentInfo } from "../../../models/RentInfo";
import { rentsScreenProvider } from "../../rents/provider/rentsScreenProvider";
import { NewRentInteractor } from "../../../interactors/newRent/newRentInteractor";

const startRent = (): Rent => ({
  buyer:{
    email: '',
    firstName: '',
    lastName: '',
  },
  employee: {
    email: '',
    firstName: '',
    lastName: '',
  },
  dateFrom: new Date(),
  dateTo: null,
  totalPrice: 0,
  rentId: 0,
  priceByDay: 0,
  numberOfMovies: 0,
  movies: []
});

export class NewRentScreenProvider extends Provider  {

  private rentInteractor: NewRentInteractor;
  private userInteractor: UserInteractor;
  private moviesInteractor: MoviesInteractor;

  constructor(
    rentInteractor: NewRentInteractor,
    userInteractor: UserInteractor,
    moviesInteractor: MoviesInteractor
  ) {
    super();
    this.rentInteractor = rentInteractor;
    this.userInteractor = userInteractor;
    this.moviesInteractor = moviesInteractor;

    this.init = this.init.bind(this);

    this.onMoviesChanged = this.onMoviesChanged.bind(this);
    this.onBuyerChanged = this.onBuyerChanged.bind(this);
    this.onEmployeeChanged = this.onEmployeeChanged.bind(this);
    this.onDateFromChanged = this.onDateFromChanged.bind(this);
    this.onSaveClicked = this.onSaveClicked.bind(this);
    this.verify = this.verify.bind(this);
  }

  private _isSaveDisabled: boolean = true;

  private _loading: boolean = true;

  private _rent: Rent = startRent();
  private _buyers: User[] = [];
  private _employees: User[] = [];
  private _movies: Movie[] = [];
  private _buyersMap: Record<string, User> = {};
  private _employeesMap: Record<string, User> = {};
  private _moviesMap: Record<string, Movie> = {};

  get rent(): Rent { return this._rent; }
  get buyers(): User[] { return this._buyers; }
  get employees(): User[] { return this._employees; }
  get movies(): Movie[] { return this._movies; }

  get isSaveDisabled(): boolean { return this._isSaveDisabled; }
  get loading(): boolean { return this._loading; }

  async init() {
    const [buyers, employees, movies] = await Promise.all([
      this.userInteractor.fetchUsers(UserType.buyer),
      this.userInteractor.fetchUsers(UserType.employee),
      this.moviesInteractor.fetchMovies(),
    ]);

    this._buyers = buyers;
    this._employees = employees;
    this._movies = movies;

    for(const buyer of buyers) {
      this._buyersMap[buyer.email] = buyer;
    }
    for(const employee of employees) {
      this._employeesMap[employee.email] = employee;
    }
    for(const movie of movies) {
      this._moviesMap[movie.movieId] = movie;
    }

    this._loading = false;
    this.notifyListener();
  }

  private verify() {

    this._isSaveDisabled = this._rent.buyer.email === '' ||
                           this._rent.employee.email === '' ||
                           this._rent.movies.length <= 0;
  }

  onDateFromChanged(date: moment.Moment | null) {
    if(date === null) return;

    this._rent.dateFrom = date.toDate();
    this.verify();
    this.notifyListener();
  }

  onBuyerChanged(email: string) {
    if(email === this._rent.buyer.email) return;

    this._rent.buyer = this._buyersMap[email];
    this.verify();
    this.notifyListener();
  }

  onEmployeeChanged(email: string) {
    if(email === this._rent.employee.email) return;

    this._rent.employee = this._employeesMap[email];
    this.verify();
    this.notifyListener();
  }

  onMoviesChanged(movies: number[]) {
    this._rent.movies = movies.map(id => this._moviesMap[id]);
    this._rent.priceByDay = movies.reduce((res, id) => res + this._moviesMap[id].priceByDay, 0);
    this.verify();
    this.notifyListener();
  }

  async onSaveClicked() {
    this._loading = true;
    this.notifyListener();
    const rentInfo: RentInfo = {
      dateFrom: moment(this._rent.dateFrom).format('YYYY-MM-DD'),
      dateTo: null,
      movies: this._rent.movies.map(m => m.movieId),
      buyer: this._rent.buyer.email ,
      employee: this._rent.employee.email ,
    }
    await this.rentInteractor.createRent(rentInfo);
    rentsScreenProvider.updateRents();

    this._loading = false;
    this.notifyListener();
  }

  protected reset(): void {
    this._isSaveDisabled = true;

    this._loading = true;

    this._rent = startRent();
    this._buyers = [];
    this._employees = [];
    this._movies = [];
    this._buyersMap = {};
    this._employeesMap = {};
    this._moviesMap = {};
  }
}

export const newRentScreenProvider = new NewRentScreenProvider(
  di.interactors.newRentInteractor as NewRentInteractor,
  di.interactors.userInteractor as UserInteractor,
  di.interactors.moviesInteractor as MoviesInteractor,
);