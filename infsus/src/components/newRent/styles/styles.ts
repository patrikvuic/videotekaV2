import { CSSProperties } from "react";

export const styles: Record<string, CSSProperties> = {
  title: {
    display: 'flex',
    justifyContent: 'space-between',
    zIndex: 10000,
  },
  card: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  },
  halfCell:  {
    width: '50%',
    textAlign: 'left',
  },
  wideCell: {
    width: '100%',
  },
  wideCellFlex: {
    width: '100%',
    display: 'flex',
    justifyContent: 'end',
  },
  divider: {
    marginTop: 0,
  },
  spanTitle: {
    fontSize: '1.5em',
  },
  loaderDiv: {
    position: 'absolute',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    top: 0,
    left: 0,
    height: '100%',
    width: '100%',
    backgroundColor: 'rgba(255, 255, 255, .35)'
  },
  select: {
    width: '100%'
  }
}