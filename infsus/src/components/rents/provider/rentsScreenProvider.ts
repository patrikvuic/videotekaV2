import { RentsInteractor } from "../../../interactors/rents/rentsInteractor"
import { Rents } from "../../../models/Rents";
import { di } from "../../../di/di";
import { Provider } from "../../../provider/provider"

export class RentsScreenProvider extends Provider {

  private rentsInteractor: RentsInteractor;

  constructor(rentsInteractor: RentsInteractor) {
    super();
    this.rentsInteractor = rentsInteractor;

    this.fetchRents = this.fetchRents.bind(this);

    this.onPageChange = this.onPageChange.bind(this);
    this.reset = this.reset.bind(this);
    this.updateRents = this.updateRents.bind(this);
  }

  private _loading: boolean = true;
  private _rentsMap: Record<string, Rents> = {};
  private _rentsPerPage: Record<string, Rents[]> = {};
  private _currentPage = 1;
  private _size = 0;
  private _activity = false;
  private _loadedPages: number[] = [];

  get rents(): Rents[] {
    return this._rentsPerPage[this._currentPage] || [];
  }
  get loading() { return this._loading; }
  get rows() { return 8; }
  get current() { return this._currentPage };
  get size() { return this._size; }
  get activity() { return this._activity; }

  init() {
    this.fetchRents(1, this.rows, false);
  }

  private async fetchRents(page: number, count: number, activity: boolean): Promise<void> {
    this._loading = true;
    this.notifyListener();

    const rentsWithInfo = await this.rentsInteractor.fetchRents(page, count, activity);

    for(var rent of rentsWithInfo.content) {
      this._rentsMap[rent.rentId] = rent;
    }
    this._size = rentsWithInfo.totalElements;
    this._loading = false;
    this._loadedPages.push(page);
    this._rentsPerPage[page] = rentsWithInfo.content;
    this.notifyListener();
  }

  updateRents() {
    this.reset();
  }

  async onPageChange(page: number) {
    if(this._loadedPages.includes(page)) {
      this._currentPage = page;
      this.notifyListener();
      return;
    }

    await this.fetchRents(page, this.rows, this._activity);
    this._currentPage = page;
    this.notifyListener();
  }

  async onActiveClicked(value: boolean) {
    this._activity = value;
    this.reset();
  }

  protected reset(): void {
    this._rentsMap = {}
    this._loading = true;
    this._size = 0;
    this._currentPage = 1;
    this._rentsPerPage = {};
    this._loadedPages = [];
    this.fetchRents(this._currentPage, this.rows, this._activity);
  }
}

export const rentsScreenProvider = new RentsScreenProvider(di.interactors.rentsInteractor as RentsInteractor);