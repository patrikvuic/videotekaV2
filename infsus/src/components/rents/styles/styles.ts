import { CSSProperties } from "react";

export const styles: Record<string, CSSProperties> = {
  pagination: {
    marginBottom: '1em',
  },
  topSpace: {
    width: '100%',
    justifyContent: 'space-between',
  }
}