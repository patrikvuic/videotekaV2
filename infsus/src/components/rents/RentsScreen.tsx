import { Table, Button, Pagination, Space, Checkbox } from 'antd';
import { Outlet, useNavigate } from 'react-router-dom';
import { Rents } from '../../models/Rents';
import { useProvider } from '../../provider/useProvider';
import { styles } from './styles/styles';
import { rentsScreenProvider } from './provider/rentsScreenProvider';

function getCols(navigate: (id: number, state: Rents) => void) {
  return [
    {
      title: 'Rent id',
      dataIndex: 'rentId',
      key: 'rentId'
    },
    {
      title: 'Date from',
      dataIndex: 'dateFrom',
      key: 'dateFrom',
      render: (date: Date) => date.toLocaleDateString('HR'),
    },
    {
      title: 'Date to',
      dataIndex: 'dateTo',
      key: 'dateTo',
      render: (date: Date) => date ? date.toLocaleDateString('HR') : '-',
    },
    {
      title: 'Price (pet day)',
      dataIndex: 'priceByDay',
      key: 'priceByDay',
      render: (price: number) => `${price} HRK`,
    },
    {
      title: 'Total price',
      dataIndex: 'totalPrice',
      key: 'totalPrice',
      render: (price: number) => price ? `${price} HRK` : '-',
    },
    {
      title: '# of movies',
      dataIndex: 'numberOfMovies',
      key: 'numberOfMovies',
    },
    {
      title: 'Buyer',
      dataIndex: 'buyer',
      render: (buyer: any) => `${buyer.firstName} ${buyer.lastName}`,
    },
    {
      title: 'Employee',
      dataIndex: 'employee',
      render: (employee: any) => `${employee.firstName} ${employee.lastName}`,
    },
    {
      title: 'Action',
      key: 'action',
      render: (text: any, record: Rents) => (
        <Button onClick={() => navigate(record.rentId, record)} >Open</Button>
      ),
    },
  ];
}

function RentsScreen() {
  const provider = useProvider(rentsScreenProvider, (prov) => prov.init());
  const navigate = useNavigate();

  return (
    <>
      <Space style={styles.topSpace}>
        <Space size={30}>
          <Pagination
            pageSize={provider.rows}
            total={provider.size}
            current={provider.current}
            onChange={provider.onPageChange}
            style={styles.pagination}
          />
          <Checkbox
            style={styles.pagination}
            onChange={(e) => provider.onActiveClicked(e.target.checked)}
            checked={provider.activity}
          >
            Active only
          </Checkbox>
        </Space>
        <Button
          style={styles.pagination}
          type="primary"
          onClick={() => navigate('/rents/create')}
        >
          Add new
        </Button>
      </Space>
      <Table
        pagination={false}
        loading={provider.loading}
        columns={getCols((id, rent) => navigate(`/rents/${id}`, {state: rent}))}
        dataSource={provider.rents.map(rent => {
          (rent as any).key = rent.rentId;

          return rent;
        })}
      />
      <Outlet />
    </>
  );
}

export default RentsScreen;