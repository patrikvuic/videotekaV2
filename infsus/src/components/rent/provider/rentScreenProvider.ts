import { di } from "../../../di/di";
import { Provider } from "../../../provider/provider"
import { RentInteractor } from "../../../interactors/rent/rentInteractor";
import { Rent } from "../../../models/Rent";
import moment from 'moment';
import { UserInteractor } from "../../../interactors/user/userInteractor";
import { User } from "../../../models/User";
import { UserType } from "../../../constants";
import { MoviesInteractor } from "../../../interactors/movie/moviesInteractor";
import { Movie } from "../../../models/Movie";
import { RentInfo } from "../../../models/RentInfo";
import { rentsScreenProvider } from "../../rents/provider/rentsScreenProvider";

export class RentScreenProvider extends Provider  {

  private rentInteractor: RentInteractor;
  private userInteractor: UserInteractor;
  private moviesInteractor: MoviesInteractor;

  constructor(
    rentInteractor: RentInteractor,
    userInteractor: UserInteractor,
    moviesInteractor: MoviesInteractor
  ) {
    super();
    this.rentInteractor = rentInteractor;
    this.userInteractor = userInteractor;
    this.moviesInteractor = moviesInteractor;

    this.getMovies = this.getMovies.bind(this);
    this.init = this.init.bind(this);

    this.onBuyersClicked = this.onBuyersClicked.bind(this);
    this.onEmployeesClicked = this.onEmployeesClicked.bind(this);
    this.onMoviesChanged = this.onMoviesChanged.bind(this);
    this.onBuyerChanged = this.onBuyerChanged.bind(this);
    this.onEmployeeChanged = this.onEmployeeChanged.bind(this);
    this.onDateFromChanged = this.onDateFromChanged.bind(this);
    this.onDateToChanged = this.onDateToChanged.bind(this);
    this.onSaveClicked = this.onSaveClicked.bind(this);
    this.onDeleteClicked = this.onDeleteClicked.bind(this);
  }

  private _isSaveDisabled: boolean = true;

  private _loading: boolean = true;
  private _loadingBuyers: boolean = false;
  private _loadingEmployees: boolean = false;
  private _loadingMovies: boolean = true;

  private _rent: Rent | null = null;
  private _buyers: User[] = [];
  private _employees: User[] = [];
  private _movies: Movie[] = [];
  private _buyersMap: Record<string, User> = {};
  private _employeesMap: Record<string, User> = {};
  private _moviesMap: Record<string, Movie> = {};

  get rent(): Rent | null { return this._rent!; }
  get buyers(): User[] { return this._buyers; }
  get employees(): User[] { return this._employees; }
  get movies(): Movie[] { return this._movies; }

  get isSaveDisabled(): boolean { return this._isSaveDisabled; }
  get loading(): boolean { return this._loading; }
  get loadingBuyers(): boolean { return this._loadingBuyers; }
  get loadingEmplyees(): boolean { return this._loadingEmployees; }
  get loadingMovies(): boolean { return this._loadingMovies; }

  async init(id: number): Promise<void> {
    const [_, rent] = await Promise.all([this.getMovies(), this.rentInteractor.fetchRent(id)])

    this._rent! = rent;
    this._loading = false;
    this._loadingMovies = false;
    this.notifyListener();
  }

  onDateFromChanged(date: moment.Moment | null) {
    if(date === null) return;

    this._rent!.dateFrom = date.toDate();
    this._isSaveDisabled = false;
    this.notifyListener();
  }

  onDateToChanged(date: moment.Moment | null) {
    this._rent!.dateTo = date ? date.toDate() : null;
    this._isSaveDisabled = false;
    this.notifyListener();
  }

  onBuyerChanged(email: string) {
    if(email === this._rent!.buyer.email) return;

    this._rent!.buyer = this._buyersMap[email];
    this._isSaveDisabled = false;
    this.notifyListener();
  }

  onEmployeeChanged(email: string) {
    if(email === this._rent!.employee.email) return;

    this._rent!.employee = this._employeesMap[email];
    this._isSaveDisabled = false;
    this.notifyListener();
  }

  onMoviesChanged(movies: number[]) {
    this._rent!.movies = movies.map(id => this._moviesMap[id]);
    this._isSaveDisabled = false;
    this._rent!.priceByDay = movies.reduce((res, id) => res + this._moviesMap[id].priceByDay, 0);
    this.notifyListener();
  }

  async onBuyersClicked() {
    if(this._buyers.length !== 0) return;

    this._loadingBuyers = true;
    this.notifyListener();

    this._buyers = await this.userInteractor.fetchUsers(UserType.buyer);
    this._loadingBuyers = false;
    this.notifyListener();

    for(const buyer of this._buyers) {
      this._buyersMap[buyer.email] = buyer;
    }
  }

  async onEmployeesClicked() {
    if(this._employees.length !== 0) return;

    this._loadingEmployees = true;
    this.notifyListener();

    this._employees = await this.userInteractor.fetchUsers(UserType.employee);
    this._loadingEmployees = false;
    this.notifyListener();

    for(const employee of this._employees) {
      this._employeesMap[employee.email] = employee;
    }
  }

  private async getMovies() {
    this._movies = await this.moviesInteractor.fetchMovies();
    this._loadingMovies = false;
    this.notifyListener();

    for(const movie of this._movies) {
      this._moviesMap[movie.movieId] = movie;
    }
  }

  async onSaveClicked() {
    this._loading = true;
    this.notifyListener();
    const rentInfo: RentInfo = {
      dateFrom: moment(this._rent!.dateFrom).format('YYYY-MM-DD'),
      dateTo: this._rent!.dateTo ? moment(this._rent!.dateTo).format('YYYY-MM-DD') : null,
      movies: this._rent!.movies.map(m => m.movieId),
      buyer: this._rent!.buyer.email,
      employee: this._rent!.employee.email,
    }
    try {
      await this.rentInteractor.updateRent(this._rent!.rentId, rentInfo);
    } catch(error) {
      alert(error);
    }
    rentsScreenProvider.updateRents();

    this._loading = false;
    this.notifyListener();
  }

  async onDeleteClicked() {
    this._loading = true;
    this.notifyListener();

    try {
      await this.rentInteractor.deleteRent(this._rent!.rentId);
    } catch(error) {
      alert(error);
    }
    rentsScreenProvider.updateRents();

    this._loading = false;
    this.notifyListener();
  }

  protected reset(): void {
    this._isSaveDisabled = true;

    this._loading = true;
    this._loadingBuyers = false;
    this._loadingEmployees = false;
    this._loadingMovies = true;

    this._rent = null;
    this._buyers = [];
    this._employees = [];
    this._buyersMap = {};
    this._employeesMap = {};
    this._moviesMap = {};
  }
}

export const rentScreenProvider = new RentScreenProvider(
  di.interactors.rentInteractor as RentInteractor,
  di.interactors.userInteractor as UserInteractor,
  di.interactors.moviesInteractor as MoviesInteractor,
);