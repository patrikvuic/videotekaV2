import { Button, Card, DatePicker, Divider, Select, Space, Popconfirm, Spin } from "antd";
import { useMemo } from "react";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { CloseOutlined } from '@ant-design/icons';
import { styles } from "./styles/styles";
import { useProvider } from "../../provider/useProvider";
import { rentScreenProvider } from "./provider/rentScreenProvider";
import { Rents } from "../../models/Rents";
import moment from "moment";
import { Rent } from "../../models/Rent";

const defaultRent: Rents = {
  buyer:{
    email: 'email@email.com',
    firstName: 'first',
    lastName: 'last',
  },
  employee: {
    email: 'email@email.com',
    firstName: 'first',
    lastName: 'last',
  },
  dateFrom: new Date(),
  dateTo: null,
  totalPrice: 0,
  rentId: 0,
  priceByDay: 0,
  numberOfMovies: 0,
}

function RentScreen() {
  const navigate = useNavigate();
  const rent = useLocation().state as Rents || defaultRent;
  const { id } = useParams<{id: string}>();

  if(!id) navigate('/rents');
  const idNum = parseInt(id!, 10);

  if(isNaN(idNum) || idNum < 0) navigate('/rents');

  const provider = useProvider(rentScreenProvider, (prov) => prov.init(idNum));

  const title = useMemo(() => {
    return <div style={styles.title}>
      <span style={styles.spanTitle}>Rent id: {provider.rent == null ? rent.rentId : provider.rent.rentId}</span>
      <Button
        shape="circle"
        icon={<CloseOutlined />}
        onClick={() => navigate('/rents')}
      />
    </div>
  }, [navigate, provider, rent]);

  async function onSave() {
    await provider.onSaveClicked();

    navigate('/rents');
  }

  async function onDelete() {
    await provider.onDeleteClicked();

    navigate('/rents');
  }

  const currentRent = provider.rent == null ? {...rent, movies: []} as Rent : provider.rent;

  if(rent == defaultRent || rent as any == {}) navigate('/rents');

  return (
    <div style={styles.modal} id="modal">
      <Card style={styles.card} title={title}>
        <Card.Grid hoverable={false} style={styles.halfCell}>
          <Divider style={styles.divider} orientation="left">Date from</Divider>
          <DatePicker
            onChange={provider.onDateFromChanged}
            value={moment(currentRent.dateFrom.toLocaleDateString('HR'), 'DD.MM.YYYY.')}
            format={'DD.MM.YYYY.'}
            allowClear={false}
          />
        </Card.Grid>
        <Card.Grid hoverable={false} style={styles.halfCell}>
          <Divider style={styles.divider} orientation="left">Buyer</Divider>
          <Select
            value={currentRent.buyer.email}
            loading={provider.loadingBuyers}
            onClick={provider.onBuyersClicked}
            onChange={provider.onBuyerChanged}
          >
            {provider.buyers.length === 0 ? (
              <Select.Option value={currentRent.buyer.email} >
                {`${currentRent.buyer.firstName} ${currentRent.buyer.lastName}`}
              </Select.Option>
            ) : (
              provider.buyers.map(buyer => (
                <Select.Option key={buyer.email} value={buyer.email} >
                  {`${buyer.firstName} ${buyer.lastName}`}
                </Select.Option>
              ))
            )}
          </Select>
        </Card.Grid>
        <Card.Grid hoverable={false} style={styles.halfCell}>
          <Divider style={styles.divider} orientation="left">Date to</Divider>
          <DatePicker
            onChange={provider.onDateToChanged}
            value={currentRent.dateTo != null
              ? moment(currentRent.dateTo.toLocaleDateString('HR'), 'DD.MM.YYYY.')
              : undefined
            }
            format={'DD.MM.YYYY.'}
            allowClear
          />
        </Card.Grid><Card.Grid hoverable={false} style={styles.halfCell}>
          <Divider style={styles.divider} orientation="left">Employee</Divider>
          <Select
            value={currentRent.employee.email}
            loading={provider.loadingEmplyees}
            onClick={provider.onEmployeesClicked}
            onChange={provider.onEmployeeChanged}
          >
            {provider.employees.length === 0 ? (
              <Select.Option value={currentRent.employee.email} >
                {`${currentRent.employee.firstName} ${currentRent.employee.lastName}`}
              </Select.Option>
            ) : (
              provider.employees.map(employee => (
                <Select.Option key={employee.email} value={employee.email} >
                  {`${employee.firstName} ${employee.lastName}`}
                </Select.Option>
              ))
            )}
          </Select>
        </Card.Grid>
        <Card.Grid hoverable={false} style={styles.halfCell}>
          <Divider style={styles.divider} orientation="left">Price (per day)</Divider>
          {currentRent.priceByDay} HRK
        </Card.Grid>
        <Card.Grid hoverable={false} style={styles.halfCell}>
          <Divider style={styles.divider} orientation="left">
            {currentRent.totalPrice ? 'Total price' : 'Current total price'}
          </Divider>
          {currentRent.totalPrice
            ? currentRent.totalPrice
            : Math.ceil((Date.now() - currentRent.dateFrom.getTime()) / (1000 * 3600 * 24)) * currentRent.priceByDay
          } HRK
        </Card.Grid>
        <Card.Grid hoverable={false} style={styles.wideCell}>
          <Divider style={styles.divider} orientation="left">Movies</Divider>
          <Select
            mode="multiple"
            style={{ width: '100%' }}
            value={provider.rent == null ? [] : currentRent.movies.map(mov => mov.movieId)}
            placeholder="Please select"
            onChange={provider.onMoviesChanged}
            loading={provider.loadingMovies}
            disabled={provider.loadingMovies}
          >
            {provider.movies.map(mov => (
              <Select.Option key={mov.movieId} value={mov.movieId} >
                {mov.title}
              </Select.Option>
            ))}
          </Select>
        </Card.Grid>
        <Card.Grid hoverable={false} style={styles.wideCellFlex}>
          <Space align="center">
            <Popconfirm
              title="Are you sure to delete this task?"
              onConfirm={onDelete}
              okText="Yes"
              cancelText="No"
            >
              <Button danger>Delete</Button>
            </Popconfirm>
          </Space>
          <Space align="center">
            <Button onClick={() => navigate('/rents')}>Cancel</Button>
            <Button onClick={onSave} type="primary" disabled={provider.isSaveDisabled}>Save</Button>
          </Space>
        </Card.Grid>
        {provider.loading && <Loader />}
      </Card>
    </div>
  );
}

function Loader() {
  return (
    <div style={styles.loaderDiv}>
      <Spin size="large" />
    </div>
  );
}

export default RentScreen;