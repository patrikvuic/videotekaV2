import { CSSProperties } from "react";

export const styles: Record<string, CSSProperties> = {
  header: {
    backgroundColor: '#ffcfd4',
  },
  content: {
    padding: '1em',
    overflowX: 'auto',
    boxSizing: 'border-box',
    minWidth: '600px',
  },
}