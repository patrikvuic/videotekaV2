import { Layout } from 'antd';
import { Content, Header } from 'antd/lib/layout/layout';
import React from 'react';
import { styles } from './styles/styles';

export type Props = React.PropsWithChildren<{
  header: React.ReactNode
}>;

function LayoutElement(props: Props) {

  return (
    <Layout>
      <Header style={styles.header}>{ props.header }</Header>
      <Content style={styles.content}>{ props.children }</Content>
    </Layout>
  )
}

export default LayoutElement;