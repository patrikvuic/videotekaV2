import React from 'react';
import { NavLink } from 'react-router-dom';
import { styles } from './styles/styles';

function NavBar() {
  return (
    <nav style={styles.nav}>
      <NavLink
        style={({ isActive }) => (!isActive ? styles.navLink : { ...styles.navLink, ...styles.activeNavLink })}
        to="/rents"
      >
        Rents
      </NavLink>
    </nav>
  )
}

export default NavBar;