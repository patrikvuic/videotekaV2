import { CSSProperties } from "react";

export const styles: Record<string, CSSProperties> = {
  nav: {
    display: 'flex',

  },
  navLink: {
    margin: '1em',
    textDecoration: 'none',
    color: 'black'
  },
  activeNavLink: {
    fontWeight: 'bold',
  }
}