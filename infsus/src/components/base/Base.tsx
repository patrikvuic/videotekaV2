import React from "react"
import LayoutElement from "../layout/LayoutElement"
import NavBar from "../navbar/NavBar"

function Base(props: React.PropsWithChildren<{}>) {
  return (
    <LayoutElement header={<NavBar />}>
      { props.children }
    </LayoutElement>
  )
}
export default Base