import { useEffect, useRef, useState } from "react";
import { Provider } from "./provider";

export function useProvider<T extends Provider>(provider: T, init?: (prov: T) => void) {
  const [_, set] = useState(false);
  const listener = useRef(() => {
    set(curr => !curr);
   });

  useEffect(() => {
    const list = listener.current;
    provider.addListener(list);
    if(init) init(provider);

    return () => provider.removeListener(list);
  }, []);

  return provider;
}