export abstract class Provider {
  constructor() {
    this.addListener.bind(this);
    this.removeListener.bind(this);
    this.reset.bind(this);
  }

  private listeners: (() => void)[] = [];

  addListener(listener: () => void) {
    if(this.listeners.includes(listener)) return;

    this.listeners.push(listener);
  }

  removeListener(listener: () => void) {
    if(!this.listeners.includes(listener)) return;

    this.listeners.splice(this.listeners.indexOf(listener), 1);
    if(this.listeners.length === 0) this.reset();
  }

  notifyListener() {
    for(const listener of this.listeners) {
      listener();
    }
  }

  protected abstract reset(): void;
}