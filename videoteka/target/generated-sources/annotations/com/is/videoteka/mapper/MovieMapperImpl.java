package com.is.videoteka.mapper;

import com.is.videoteka.entity.Movie;
import com.is.videoteka.response.MovieResponse;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-05-22T18:44:41+0200",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.1 (Homebrew)"
)
@Component
public class MovieMapperImpl extends MovieMapper {

    @Override
    public MovieResponse toMovieResponse(Movie movie) {
        if ( movie == null ) {
            return null;
        }

        MovieResponse movieResponse = new MovieResponse();

        if ( movie.getId() != null ) {
            movieResponse.setMovieId( movie.getId() );
        }
        if ( movie.getTitle() != null ) {
            movieResponse.setTitle( movie.getTitle() );
        }
        if ( movie.getDescription() != null ) {
            movieResponse.setDescription( movie.getDescription() );
        }
        if ( movie.getPriceByDay() != null ) {
            movieResponse.setPriceByDay( movie.getPriceByDay() );
        }
        if ( movie.getThumbnailUrl() != null ) {
            movieResponse.setThumbnailUrl( movie.getThumbnailUrl() );
        }

        movieResponse.setGenreTitle( getGenreTitle(movie) );
        movieResponse.setActors( getActors(movie) );

        return movieResponse;
    }
}
