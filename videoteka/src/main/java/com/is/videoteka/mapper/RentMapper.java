package com.is.videoteka.mapper;

import com.is.videoteka.entity.Movie;
import com.is.videoteka.entity.Rent;
import com.is.videoteka.entity.User;
import com.is.videoteka.response.MovieResponse;
import com.is.videoteka.response.RentDetailsResponse;
import com.is.videoteka.response.RentResponse;
import com.is.videoteka.response.UserResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(
        componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS
)
@Component
public abstract class RentMapper {

    private UserMapper userMapper;
    private MovieMapper movieMapper;

    @Autowired
    public void setUserMapper(UserMapper userMapper){
        this.userMapper = userMapper;
    }
    @Autowired
    public void setMovieMapper(MovieMapper movieMapper){
        this.movieMapper = movieMapper;
    }

    @Mapping(target = "buyer", expression = "java(mapUser(rent.getBuyer()))")
    @Mapping(target = "employee", expression = "java(mapUser(rent.getEmployee()))")
    @Mapping(target = "movies", expression = "java(mapMovies(rent))")
    @Mapping(target = "priceByDay", expression = "java(getPriceByDay(rent))")
    @Mapping(target = "rentId", source = "rent.id")
    public abstract RentDetailsResponse toRentDetailsResponse(Rent rent);

    @Mapping(target = "buyer", expression = "java(mapUser(rent.getBuyer()))")
    @Mapping(target = "employee", expression = "java(mapUser(rent.getEmployee()))")
    @Mapping(target = "numberOfMovies", expression = "java(getNumberOfMovies(rent))")
    @Mapping(target = "priceByDay", expression = "java(getPriceByDay(rent))")
    @Mapping(target = "rentId", source = "rent.id")
    public abstract RentResponse toRentResponse(Rent rent);

    protected UserResponse mapUser(User user){
        return userMapper.toUserResponse(user);
    }

    protected Integer getNumberOfMovies(Rent rent){
        return rent.getRentedMovies().size();
    }

    protected Double getPriceByDay(Rent rent){
        return rent.getRentedMovies().stream().mapToDouble(Movie::getPriceByDay).sum();
    }

    protected List<MovieResponse> mapMovies(Rent rent){
        return rent.getRentedMovies().stream().map(movieMapper::toMovieResponse).collect(Collectors.toList());
    }

}
