package com.is.videoteka.mapper;

import com.is.videoteka.entity.Movie;
import com.is.videoteka.response.MovieResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Mapper(
        componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS
)
@Component
public abstract class MovieMapper {

    @Mapping(target = "genreTitle", expression = "java(getGenreTitle(movie))")
    @Mapping(target = "actors", expression = "java(getActors(movie))")
    @Mapping(target = "movieId", source = "movie.id")
    public abstract MovieResponse toMovieResponse(Movie movie);

    protected String getGenreTitle(Movie movie){
        return movie.getGenre().getTitle();
    }

    protected List<String> getActors(Movie movie){
        return Arrays.asList(movie.getActorsList().split(", "));
    }
}
