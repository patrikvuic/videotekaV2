package com.is.videoteka.response;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RentResponse {

    private Long rentId;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private Double totalPrice;
    private Double priceByDay;
    private UserResponse buyer;
    private UserResponse employee;
    private Integer numberOfMovies;
}
