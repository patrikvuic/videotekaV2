package com.is.videoteka.controller;

import com.is.videoteka.request.RentRequest;
import com.is.videoteka.response.RentDetailsResponse;
import com.is.videoteka.response.RentResponse;
import com.is.videoteka.service.RentService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rents")
@RequiredArgsConstructor
public class RentController {

    private final RentService rentService;

    @GetMapping
    public ResponseEntity<Page<RentResponse>> getRents(
            @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "25") Integer size,
            @RequestParam(value = "activeOnly", required = false) boolean activeOnly
    ) {
        Pageable pageable = PageRequest.of(page, size);
        return ResponseEntity.ok(rentService.getRents(pageable, activeOnly));
    }

    @GetMapping("/{id}")
    public ResponseEntity<RentDetailsResponse> getRentById(@PathVariable Long id){
        return ResponseEntity.ok(rentService.getRentById(id));
    }

    @PostMapping
    public ResponseEntity<Long> createNewRent(@RequestBody RentRequest rentRequest){
        return new ResponseEntity<Long>(rentService.createNewRent(rentRequest), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRent(@PathVariable Long id){
        rentService.deleteRent(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<RentDetailsResponse> updateRent(@RequestBody RentRequest request,
                                                   @PathVariable Long id){
        return ResponseEntity.ok(rentService.updateRent(id, request));
    }
}
