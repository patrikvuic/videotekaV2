package com.is.videoteka.repository;

import com.is.videoteka.entity.Rent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface RentRepository extends PagingAndSortingRepository<Rent, Long> {

    Page<Rent> findAllByDateToAfter(LocalDate current, Pageable pageable);
    Page<Rent> findAllByDateToIsNull(Pageable pageable);

}
