package com.is.videoteka.repository;

import com.is.videoteka.entity.User;
import com.is.videoteka.entity.util.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    List<User> findAllByRole(UserRole role);
}
