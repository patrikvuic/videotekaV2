package com.is.videoteka.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Film")
@Data
public class Movie {

    @Id
    @Column(name = "siffilm")
    private Long id;

    @Column(name = "naziv")
    private String title;

    @Column(name = "opis")
    private String description;

    @Column(name = "glumci")
    private String actorsList;

    @Column(name = "cijenapodanu")
    private Double priceByDay;

    @Column(name = "urlslika")
    private String thumbnailUrl;

    @ManyToOne
    @JoinColumn(name = "sifkategorija", nullable = false)
    @JsonBackReference
    private Genre genre;
}
