package com.is.videoteka.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "najam")
@Data
public class Rent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sifnajam")
    private Long id;

    @Column(name = "datumod")
    private LocalDate dateFrom;

    @Column(name = "datumdo")
    private LocalDate dateTo;

    @Column(name = "ukcijena")
    private Double totalPrice;

    @ManyToOne
    @JoinColumn(name = "emailkupac")
    private User buyer;

    @ManyToOne
    @JoinColumn(name = "emailzaposlenik", nullable = false)
    private User employee;

    @ManyToMany
    @JoinTable(
            name = "stavkanajma",
            joinColumns = @JoinColumn(name = "sifnajam"),
            inverseJoinColumns = @JoinColumn(name = "siffilm")
    )
    Set<Movie> rentedMovies;

}
