package com.is.videoteka.request;

import lombok.Data;

import java.time.LocalDate;
import java.util.Set;

@Data
public class RentRequest {

    private LocalDate dateFrom;
    private LocalDate dateTo;
    private String buyer;
    private String employee;
    private Set<Long> movies;
}
