package com.is.videoteka.service.impl;

import com.is.videoteka.entity.Movie;
import com.is.videoteka.exception.NotFoundException;
import com.is.videoteka.mapper.MovieMapper;
import com.is.videoteka.repository.MovieRepository;
import com.is.videoteka.response.MovieResponse;
import com.is.videoteka.service.MovieService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MovieServiceImpl implements MovieService {

    private final MovieRepository movieRepository;
    private final MovieMapper movieMapper;

    @Override
    public List<MovieResponse> getAllMovies() {
        List<Movie> movies = movieRepository.findAll();
        return movies.stream().map(movieMapper::toMovieResponse).collect(Collectors.toList());
    }

    @Override
    public Set<Movie> findByIds(Set<Long> movieIds) {
        Set<Movie> movies =  movieRepository.findAllByIdIn(movieIds);
        if(movies.size() < movieIds.size()) throw new NotFoundException("Some of the movies are not found.");
        return movies;
    }
}
