package com.is.videoteka.service;

import com.is.videoteka.entity.User;
import com.is.videoteka.entity.util.UserRole;
import com.is.videoteka.response.UserResponse;

import java.util.List;

public interface UserService {

    List<UserResponse> getUsersByRole(UserRole role);
    User findByEmail(String email);
}
